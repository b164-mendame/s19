console.log("arigathanks")

//3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…

let num = 2
let exponent = 3

console.log(`The cube of ${num} is ${num ** exponent}`)


//5. Create a variable address with a value of an array containing details of an address.


const address = ["258", "Washington Ave NW", "California" , "90011"];


//6. Destructure the array and print out a message with the full address using Template Literals.

const[houseNumber, street, state, zipCode] = address

console.log(`Hello, I live at ${houseNumber} ${street}, ${state} ${zipCode}.`)

//7. Create a variable animal with a value of an object data type with different animal details as it’s properties.

const squirtle = {
	weight: "20kg",
	height: "3ft",
	level: "5"
}


// 8. Destructure the object and print out a message with the details of the animal using Template Literals.

const {weight, height, level } = squirtle;

console.log(`Squirtle is a level ${level} pokemon, with a height of ${height} and weight of ${weight}.`)

// 9. Create an array of numbers.

let array = [1,2,3,4,5,15]



//10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

array.forEach(number => console.log(number))

//11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
 
 let reduceArray = array.reduce((x,y) => x + y)
 console.log(reduceArray)

//12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
//13. Create/instantiate a new object from the class Dog and console log the object.


class Dog {
	constructor(breed, name, age) {
		this.breed = breed;
		this.name = name;
		this.age = age;
	}
}

const myDog = new Dog("Labrador", "Ellie", 10);

console.log(myDog);

